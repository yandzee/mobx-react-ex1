import _ from 'lodash'

export const camelCasify = (obj: object): object => {
  if (_.isArray(obj)) {
    return obj.map((v: any) => camelCasify(v));
  }

  if (_.isFunction(obj)) return obj;

  if (_.isObject(obj)) {
    return _.entries(obj).reduce((acc: any, pair: [string, any]) => {
      const camelCasedKey = _.camelCase(pair[0]);
      const camelCasedValue = camelCasify(pair[1]);

      return Object.assign(acc, {
        [camelCasedKey]: camelCasedValue
      });
    }, {});
  }

  return obj;
};
