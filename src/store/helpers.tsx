import React, { FunctionComponent } from 'react'
import { Store } from './'
import { useLocalStore } from 'mobx-react'

const StoreContext = React.createContext<Store | null>(null)

export const StoreProvider: FunctionComponent<{}> = ({ children }) => {
  const store = useLocalStore(Store.new);

  return (
    <StoreContext.Provider value={store}>
      {children}
    </StoreContext.Provider>
   );
}

export const useStore = () => {
  const store = React.useContext(StoreContext)
  if (!store) {
    // this is especially useful in TypeScript so you don't need to be checking for null all the time
    throw new Error('useStore must be used within a StoreProvider.')
  }
  return store
}
