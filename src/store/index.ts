import { observable, autorun } from 'mobx'

import EntriesStore from './entries'
import { useStore, StoreProvider } from './helpers'
export { useStore, StoreProvider }

import { IEntry , EntryKind } from '@js/domain/entry'

export class Store {
  @observable
  public entries: EntriesStore;

  constructor() {
    this.entries = new EntriesStore();

    autorun(() => {
      const n = this.entries.length;

      console.log(`Entries changed, current length: ${n}`);
    });
  }

  static new(): Store {
    return new Store();
  }

  addEntry(kind: EntryKind, data: string) {
    this.entries.addEntry(kind, data);
  }
}
