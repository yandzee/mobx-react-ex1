import { observable } from 'mobx'

import { IEntry , EntryKind } from '@js/domain/entry'

export default class EntriesStore {
  @observable
  public entries: Array<IEntry>;

  constructor() {
    this.entries = [];
  }

  addEntry(kind: EntryKind, data: string) {
    this.entries.push({ kind, data });
  }

  get length(): number {
    return this.entries.length;
  }
}
