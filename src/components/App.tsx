import React, { FunctionComponent, useState } from 'react'
import { observer } from 'mobx-react'

import { useStore } from '@js/store'
import { EntryKind } from '@js/domain/entry'

const app: FunctionComponent<{}> = observer(() => {
  const [k, setK] = useState(0);

  const store = useStore();
  const { entries } = store;

  const elems = entries.entries.map((e, i) => (
    <li key={i}>
      Kind: {e.kind}, data: {e.data}
    </li>
  ));

  const addElem = () => {
    const kind = Math.random() > 0.5 ? EntryKind.Process : EntryKind.Function;
    entries.addEntry(kind, `${k}`);

    setK(k + 1);
  };


  return (
    <div>
      <ul>
        {elems}
      </ul>

      <div className="add" onClick={addElem}>
        AddElem
      </div>
    </div>
  );
});

export default app
