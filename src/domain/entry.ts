export enum EntryKind {
  Process = 'process',
  Function = 'function',
}

export interface IEntry {
  kind: EntryKind;
  data: string;
}
