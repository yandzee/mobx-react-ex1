import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { useStore, StoreProvider } from '@js/store'

import App from './components/App'
import './styles/index.scss'

const render = () => {
  const e = (
    <StoreProvider>
      <App></App>
    </StoreProvider>
  );

  ReactDOM.render(e, document.getElementById('app'));
};

render();
